module.exports = (sequelize, type) => {
    const Wallets = sequelize.define('wallets', {
        id_wallet:{
            type: type.INTEGER, 
            primaryKey:true,
            autoIncrement:true
        },
        codigo_wallet:{
            type: type.STRING(100)
        },
        imagen_qr:{
            type: type.STRING(150)
        },
        email:{
            type: type.STRING(200)
        },
        id_cryptomoneda:{
            type: type.STRING(250)
        }
    },
    {
        timestamps: false
    })
    return Wallets;
}