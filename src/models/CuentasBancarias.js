module.exports = (sequelize, type) => {
    const CuentasBancarias = sequelize.define('cuentas_bancarias', {
        id_cuenta_bancaria:{
            type: type.INTEGER, 
            primaryKey:true,
            autoIncrement: true
        },
        email:{
            type: type.STRING(100)
        },
        slug_bank:{
            type: type.STRING(20)
        },
        titular:{
            type: type.STRING(250)
        },
        nro_cuenta:{
            type: type.STRING(150)
        },
        moneda:{
            type: type.STRING(10)
        }
    },
    {
        timestamps: false
    })
    return CuentasBancarias;
}