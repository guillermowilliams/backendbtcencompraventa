module.exports = (sequelize, type) => {
    const Bancos = sequelize.define('bancos', {
        slug_bank:{
            type: type.STRING(20), 
            primaryKey:true,
        },
        banco_shortname:{
            type: type.STRING(10)
        },
        banco_largename:{
            type: type.STRING(250)
        }
    },
    {
        timestamps: false
    })
    return Bancos;
}