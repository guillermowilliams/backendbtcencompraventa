module.exports = (sequelize, type) => {
    const Cryptomoneda = sequelize.define('cryptomoneda', {
        id_cryptomoneda:{
            type: type.INTEGER, 
            primaryKey:true,
            autoIncrement: true,
            allowNull: false
        },
        descripcion_larga:{
            type: type.STRING(50)
        },
        descripcion_corta:{
            type: type.STRING(10)
        },
        logo:{
            type: type.TEXT
        }
    },
    {
        timestamps: false
    })
    return Cryptomoneda;
}