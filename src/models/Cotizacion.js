module.exports = (sequelize, type) => {
    const Cotizacion = sequelize.define('cotizacion', {
        id_cotizacion:{
            type: type.UUID,
            defaultValue: type.UUIDV4, 
            primaryKey:true
        },
        email:{
            type: type.STRING(150)
        },
        id_cryptomoneda:{
            type: type.INTEGER
        },
        cantidad:{
            type: type.FLOAT
        },
        monto:{
            type: type.FLOAT
        },
        moneda:{
            type: type.STRING(10)
        },
        modalidad:{
            type: type.STRING(10)
        },
        valorcripto:{
            type: type.FLOAT
        },
        valordolar:{
            type: type.FLOAT
        },
        date_register:{
            type: type.STRING(20)
        },
        date_expired:{
            type: type.STRING(20)
        },
        status:{
            type: type.STRING(1),
            defaultValue: '1'
        }
    },
    {
        timestamps: false
    })
    return Cotizacion;
}