module.exports = (sequelize, type) => {
    const Usuario = sequelize.define('usuario', {
        id_usuario:{
            type: type.UUID,
            defaultValue: type.UUIDV4, 
            primaryKey:true
        },
        email:{
            type: type.STRING(100),
            allowNull: false
        },
        nombres:{
            type: type.STRING(100)
        },
        apellidos:{
            type: type.STRING(100)
        },
        codigo_pais:{
            type: type.STRING(13),
            defaultValue: '51'
        },
        celular:{
            type: type.STRING(15)
        },
        direccion:{
            type: type.STRING(150)
        },
        password:{
            type: type.STRING
        },
        imgUser:{
            type: type.STRING,
            defaultValue: '/userImg.png'
        },
        typeDoc:{
            type: type.STRING(10)
        },
        numDoc: {
            type: type.STRING(15)
        },
        reniecValidation: {
            type: type.BOOLEAN(true),
            defaultValue: 0
        },
        imgIdentityFace:{
            type: type.STRING
        },
        imgIdentityStamp:{
            type: type.STRING
        },
        imgIdentitySelfie:{
            type: type.STRING
        },
        imgValidation:{
            type: type.SMALLINT,
            defaultValue: 0 
        },
        resetToken:{
            type: type.TEXT
        },
        activacion:{
            type: type.STRING(300)
        },
        estado:{
            type: type.SMALLINT,
            defaultValue: 0 
        }
    },{
        timestamps: true
    })
    return Usuario;
}