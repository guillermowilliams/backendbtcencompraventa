module.exports = (sequelize, type) => {
    const ConfigPage = sequelize.define('configpages', {
        id_configpage:{
            type: type.INTEGER,
            primaryKey:true,
            autoIncrement:true
        },
        p_compra:{
            type: type.FLOAT
        },
        p_venta:{
            type: type.FLOAT
        },
        host_email:{
            type: type.STRING(150)
        },
        host_wallet:{
            type: type.STRING(100)
        },
        cts_pen:{
            type: type.TEXT
        },
        cts_usd:{
            type: type.TEXT
        },
        wallet_qr:{
            type: type.STRING(300)
        }
    },
    {
        timestamps: false
    })
    return ConfigPage;
}