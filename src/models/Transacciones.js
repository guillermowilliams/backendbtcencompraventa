module.exports = (sequelize, type) => {
    const Bancos = sequelize.define('transacciones', {
        id_transaccion:{
            type: type.INTEGER(5).ZEROFILL, 
            primaryKey:true,
            autoIncrement:true
        },
        id_cotizacion:{
            type: type.UUID,
            defaultValue: type.UUIDV4,
        },
        id_cryptomoneda:{
            type: type.INTEGER
        },
        cantidad_transaccion:{
            type: type.FLOAT
        },
        total_transaccion:{
            type: type.FLOAT
        },
        codigo_wallet:{
            type: type.STRING(100)
        },
        slug_bank:{
            type: type.STRING(20)
        },
        cta_bank:{
            type: type.STRING(20)
        },
        img_voucher:{
            type: type.STRING(255)
        },
        email:{
            type: type.STRING(100)
        },
        moneda:{
            type: type.STRING(15)
        },
        tipo_transaccion:{
            type: type.STRING(50)
        },
        status_transaccion:{
            type: type.STRING(15),
            defaultValue: 'verificando'
        },
        observacion:{
            type: type.TEXT
        },
        date_register:{
            type: type.DATE,
            defaultValue: type.NOW
        },
        date_confirmation:{
            type: type.STRING(20)
        },
        date_delete:{
            type: type.STRING(20)
        }    
    },
    {
        timestamps: false
    })
    return Bancos;
}