const md5 = require('md5');
const tinify = require("tinify");
tinify.key = "RxrcHXTnMvnFyhyK9y781J1Z3LqGyn9L";

function uploadFiles(img, folder, callback, nameFile){
    let imgDetail = {
        name        :   (nameFile ? nameFile : md5(img.name)),
        size        :   img.size,
        extension   :   img.name.split('.').pop()
    }
    let response = { 
        status:false, 
        directorio:folder+imgDetail.name+'.'+imgDetail.extension 
    };
    img.mv('./public/'+folder+imgDetail.name+'.'+imgDetail.extension, function(err) {
        console.log(err)
    })
    setTimeout(() => {
        const source = tinify.fromFile('./public/'+folder+imgDetail.name+'.'+imgDetail.extension);
        source.toFile('./public/'+folder+imgDetail.name+'.'+imgDetail.extension, function(err){
            if (!err) {
                response.status = true;
            }
        });
    }, 1000);        
    return callback(response) 
}

module.exports = uploadFiles;