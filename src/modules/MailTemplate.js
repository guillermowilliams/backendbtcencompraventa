function MailTemplate(cod){
    return `
    <html>
        <head>
            <style>
                .txt1 {
                    font-size: 30px;
                }

                .btn {
                    font-size: 18px;
                }

                .link {
                    font-size: 16px;
                }

                .img-header {
                    margin-bottom: 50px;
                }

                .footer {
                    padding-bottom: 35px;
                }

                @media screen and (max-width: 500px) {
                    .txt1 {
                        font-size: 20px;
                    }

                    .txt2 {
                        font-size: 10px;
                    }

                    .btn {
                        font-size: 13px;
                    }

                    .link {
                        font-size: 10px;
                    }

                    .img-header {
                        margin-bottom: 40px;
                    }

                    .footer {
                        padding-bottom: 25px;
                    }
                }
            </style>
        </head>

        <body style="padding:0;margin:0;width: 100%;background-color: #F6F6F6;">
            <table width="450" style="margin: auto;margin-bottom: 20px;" cellspacing="0" rowspacing="0">
                <tr style="background: #fff;">
                    <td class="txt1" style="
                            text-align: center;
                            font-style: normal;
                            font-weight: bold;
                            color: #212121;
                            line-height: 120%;
                            font-family: tahoma, verdana, segoe, sans-serif;
                            ">
                        <a href="${process.env.FRONT_URL}" target="_blank">
                            <img width="100%" src="https://service.btcencompraventa.com/Mailing/mailHeader.png"
                                style="border-top-left-radius: 30px;border-top-right-radius: 30px;" class="img-header">
                        </a>
                        Verificación de correo<br>electrónico
                    </td>
                </tr>
                <tr style="background: #fff;">
                    <td class="txt2" style="
                            text-align: center;
                            color: #495057;
                            font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;
                            padding-top: 20px;
                            ">
                        <strong>De click en el botón para verificar su correo electrónico</strong>
                    </td>
                </tr>
                <tr style="background: #fff;">
                    <td style="text-align: center;padding-top: 30px;">
                        <a href="${process.env.FRONT_URL+'activacion/'+cod}" class="btn" target="_blank" style="
                                background: #ffa51d;
                                color: #fff;
                                border-radius: 26px;
                                font-weight: normal;
                                font-family: roboto, helvetica neue, helvetica, arial, sans-serif;
                                text-decoration: none;
                                padding: 10px 20px 10px 20px;
                                ">
                            Click aquí para verificar correo
                        </a>
                    </td>
                </tr>
                <tr style="background: #fff;">
                    <td class="txt2" style="
                                text-align: center;
                                color: #131313;
                                font-family: roboto, helvetica neue, helvetica, arial, sans-serif;
                                padding-top: 30px;
                            ">
                        <strong>o copie el siguiente link en su navegador</strong>
                    </td>
                </tr>
                <tr style="background: #fff;">
                    <td style="text-align: center;width:300px;text-size-adjust: 100%;padding-top:30px;padding-left: 10px;
                            padding-right: 10px;" class="footer">
                        <a href="${process.env.FRONT_URL+'activacion/'+cod}" class="link" target="_blank" style="
                                    color: #3d85c6;
                                    font-family: roboto, helvetica neue, helvetica, arial, sans-serif;
                                    text-decoration: underline;
                                    word-wrap: break-word;
                                ">
                            <strong>
                                ${process.env.FRONT_URL+'activacion/'+cod}
                            </strong>
                        </a>
                    </td>
                </tr>
                <tr style="background: #fff;">
                    <td>
                        <div
                            style="background-image: url(https://service.btcencompraventa.com/Mailing/bg-footer3.png);background-size:contain;background-repeat: no-repeat;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;padding-top: 17%;">
                            <div
                                style="width: 100%;display: flex;margin: auto;padding: 15px 0px;background-color: #05182d;justify-content: center;">
                                <div style="width: fit-content; margin: auto;">
                                    <a href="#" target="_blank" style="text-decoration: none;">
                                        <img style="width: 32px;"
                                            src="https://service.btcencompraventa.com/Mailing/facebook-circle-gray-bordered.png">
                                    </a>
                                    <a href="#" target="_blank" style="margin-left: 10px;text-decoration: none;">
                                        <img style="width: 32px;"
                                            src="https://service.btcencompraventa.com/Mailing/twitter-circle-gray-bordered.png">
                                    </a>
                                    <a href="#" target="_blank" style="margin-left: 10px;text-decoration: none;">
                                        <img style="width: 32px;"
                                            src="https://service.btcencompraventa.com/Mailing/instagram-circle-gray-bordered.png">
                                    </a>
                                    <a href="#" target="_blank" style="margin-left: 10px;text-decoration: none;">
                                        <img style="width: 32px;"
                                            src="https://service.btcencompraventa.com/Mailing/youtube-circle-gray-bordered.png">
                                    </a>
                                </div>
                            </div>
                            <div
                                style="text-align: center;color:#fff;font-family: roboto, helvetica neue, helvetica, arial, sans-serif;font-size: 12px;padding-top: 20px;padding-bottom: 20px;background-color: #05182d;border-bottom-left-radius: 30px;border-bottom-right-radius: 30px;">
                                © Todos los derechos reservados.
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </body>
    </html>
    `;
}

module.exports = MailTemplate;