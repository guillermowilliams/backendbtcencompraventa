const md5 = require('md5');
const sha256 = require('sha256');

function apiToken(session_id){
    let key = md5(process.env.SITE_KEY+session_id);
    return sha256(key);
}

module.exports = apiToken;

