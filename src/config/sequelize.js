const Sequelize = require('sequelize');
const UsuarioModel = require('../models/Usuario');
const CotizacionModel = require('../models/Cotizacion');
const CryptomonedaModel = require('../models/Cryptomoneda');
const CuentasBancariasModel = require('../models/CuentasBancarias');
const BancosModel = require('../models/Bancos');
const WalletModel = require('../models/Wallets');
const TransaccionesModel = require('../models/Transacciones');
const ConfigPageModel = require('../models/ConfigPage');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql',
})

const db = {};

db.usuario = UsuarioModel(sequelize, Sequelize);
db.cotizacion = CotizacionModel(sequelize, Sequelize);
db.cryptomoneda = CryptomonedaModel(sequelize, Sequelize);
db.cuentas_bancarias = CuentasBancariasModel(sequelize, Sequelize);
db.bancos = BancosModel(sequelize, Sequelize);
db.wallet = WalletModel(sequelize, Sequelize);
db.transacciones = TransaccionesModel(sequelize, Sequelize);
db.configpage = ConfigPageModel(sequelize, Sequelize);

//Relations

//Cotizacion - Cryptomoneda
db.cotizacion.belongsTo(db.cryptomoneda, { foreignKey: 'id_cryptomoneda', constraints: false});
db.cryptomoneda.hasMany(db.cotizacion, { foreignKey: 'id_cryptomoneda', constraints: false});
//Cotizacion - Usuario
db.cotizacion.belongsTo(db.usuario, { foreignKey: 'email', constraints: false});
db.usuario.hasMany(db.cotizacion, { foreignKey: 'email', constraints: false});
//Cuentas bancarias - Banco
db.cuentas_bancarias.belongsTo(db.bancos, { foreignKey: 'slug_bank', constraints: false});
db.bancos.hasMany(db.cuentas_bancarias, { foreignKey: 'slug_bank', constraints: false});
//Cuentas bancarias - Usuario
db.cuentas_bancarias.belongsTo(db.usuario, { foreignKey: 'email', constraints: false});
db.usuario.hasMany(db.cuentas_bancarias, { foreignKey: 'email', constraints: false});
//Cuentas bancarias - Usuario
db.cuentas_bancarias.belongsTo(db.usuario, { foreignKey: 'email', constraints: false});
db.usuario.hasMany(db.cuentas_bancarias, { foreignKey: 'email', constraints: false});
//Wallet - Usuario
db.wallet.belongsTo(db.usuario, { foreignKey: 'email', constraints: false});
db.usuario.hasMany(db.wallet, { foreignKey: 'email', constraints: false});
//Wallet - cryptomoneda
db.wallet.belongsTo(db.cryptomoneda, { foreignKey: 'id_cryptomoneda', constraints: false});
db.cryptomoneda.hasMany(db.wallet, { foreignKey: 'id_cryptomoneda', constraints: false});
//Transacciones - cryptomoneda
db.transacciones.belongsTo(db.cryptomoneda, { foreignKey: 'id_cryptomoneda', constraints: false});
db.cryptomoneda.hasMany(db.transacciones, { foreignKey: 'id_cryptomoneda', constraints: false});
//Transacciones - bancos
db.transacciones.belongsTo(db.bancos, { foreignKey: 'slug_bank', constraints: false});
db.bancos.hasMany(db.transacciones, { foreignKey: 'slug_bank', constraints: false});
//Transacciones - usuarios
db.transacciones.belongsTo(db.usuario, { foreignKey: 'email', constraints: false});
db.usuario.hasMany(db.transacciones, { foreignKey: 'email', constraints: false});
//Transacciones - cotizacion
db.transacciones.belongsTo(db.cotizacion, { foreignKey: 'id_cotizacion', constraints: false});
db.cotizacion.hasMany(db.transacciones, { foreignKey: 'id_cotizacion', constraints: false});

sequelize.sync().then(() => {
    console.log('Tablas creadas.')
})

module.exports = db;