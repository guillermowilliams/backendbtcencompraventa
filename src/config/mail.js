const nodemailer = require("nodemailer");

class Email{

    constructor(){
        this.createTransport = nodemailer.createTransport({
            host    :   process.env.MAILING_HOST,
            secure  :   process.env.MAILING_SECURE,
            auth:{
                user:   process.env.MAILING_USER,
                pass:   process.env.MAILING_PASS
            }
        });
    }

    enviarCorreo(oEmail){
        try {
            this.createTransport.sendMail(oEmail, function(error, info){
                if (error) {
                    console.log("Error al enviar email");
                }else{
                    console.log("Correo enviado correctamente");
                }
                this.createTransport.close();
            })
        } catch (error) {
            console.log(error)
        }
    }

}

module.exports = Email;