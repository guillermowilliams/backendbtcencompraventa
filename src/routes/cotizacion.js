const express = require('express');
const router  = express.Router();
const verifyToken = require('../config/verifyToken') 
const db = require('./../config/sequelize');

function fnSetearHora(expired = false){
    let d = new Date();
        if (expired) {
            d.setMinutes(d.getMinutes() + 30)
        }
        let day = d.getDate();
        let month = d.getMonth() + 1;
        let hours = d.getHours();
        let minutes = d.getMinutes();
        let seconds = d.getSeconds();
        day = (day < 10) ? '0' + day : day;
        month = (month < 10) ? '0' + month : month;
        hours = (hours < 10) ? '0' + hours : hours;
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        seconds = (seconds < 10) ? '0' + seconds : seconds;

        let fecha_hora_actual = d.getFullYear() +'-'+ month +'-'+ day +' ' +hours+ ':'+minutes+':'+seconds;
        return fecha_hora_actual;
}

router.post(process.env.URL_ROUTES+'cotizacion', verifyToken, (req, res) => {
    const { 
        email,
        cantidad, 
        monto, 
        moneda,
        modalidad,
        valorcripto,
        valordolar
    } = req.body;
    
    db.cotizacion.create({
        email: email,
        id_cryptomoneda: 1,
        cantidad: cantidad,
        monto: monto,
        moneda: moneda,
        modalidad: modalidad,
        valorcripto: valorcripto,
        valordolar: valordolar,
        date_register: fnSetearHora(),
        date_expired: fnSetearHora(true)
    }).then(cotizacion => {
        if (cotizacion) {
            res.json({ status:true , message:cotizacion.dataValues.id_cotizacion })
        }else{
            res.json({ status:false , message:'Hubo un error al guardar información' })
        }
    }).catch(error => {
        res.json({ status:false , message:'Error al ejecutar función' })
    })
})

router.post(process.env.URL_ROUTES+'cotizacion-info', verifyToken, (req, res) =>{
    const {email, id_cotizacion} = req.body;

    db.cotizacion.findOne({
        attributes:[
            'cantidad',
            'monto',
            'moneda',
            'modalidad',
            'date_register',
            'date_expired',
            'status'
        ],
        include: [
            {
                model: db.cryptomoneda,
                attributes: ['id_cryptomoneda','descripcion_larga','descripcion_corta','logo']
            },
            {
                model: db.usuario,
                attributes: ['email','nombres','apellidos','celular','direccion','typeDoc','numDoc','imgValidation','activacion','estado']
            }
        ],
        where:{ 
            email:email,
            id_cotizacion:id_cotizacion
        }
    }).then(cotizacion => {
        if (cotizacion) {
            res.json({ status:true , message:'Mostrando información', info:cotizacion })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

module.exports = router;