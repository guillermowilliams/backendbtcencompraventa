const express = require('express');
const router  = express.Router();
const verifyToken = require('../config/verifyToken') 
const db = require('./../config/sequelize');

router.post(process.env.URL_ROUTES+'wallets', verifyToken, (req, res) =>{
    const { email } = req.body;

    db.wallet.findAll({
        attributes:[
            'id_wallet',
            'codigo_wallet',
            'imagen_qr'
        ],
        include: [
            {
                model: db.cryptomoneda,
                attributes:[
                    'descripcion_larga',
                    'descripcion_corta',
                    'logo'
                ],
            }
        ],
        where:{ email:email }
    }).then(wallets => {
        if (wallets) {
            res.json({ status:true , message:'Mostrando información', info:wallets })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'deletewallet', verifyToken, (req, res) =>{
    const { email, id } = req.body;

    db.wallet.destroy({
        where: {
          email: email,
          id_wallet: id
        }
    }).then(wallet => {
        if (wallet) {
            res.json({ status:true , message:'Wallet eliminada correctamente', info:wallet })
        }else{
            res.json({ status:false , message:'No se pudo eliminar la wallet seleccionada' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    });
})

router.post(process.env.URL_ROUTES+'addwallet', verifyToken, (req, res) =>{
    const { email, codigo_wallet } = req.body;

    db.wallet.create({
        codigo_wallet:codigo_wallet, 
        email: email, 
        id_cryptomoneda: 1
    }).then(walletCreate => {
        if (walletCreate) {
            res.json({ status:true , message:'Wallet agregada', info:walletCreate })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

module.exports = router;