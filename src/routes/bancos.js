const express = require('express');
const router  = express.Router();
const verifyToken = require('../config/verifyToken') 
const db = require('./../config/sequelize');

router.get(process.env.URL_ROUTES+'bancos', verifyToken, (req, res) =>{
    db.bancos.findAll().then(bancos => {
        if (bancos) {
            res.json({ status:true , message:'Mostrando información', info:bancos })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'bancos', verifyToken, (req, res) =>{
    const { slug_bank, moneda } = req.body;
    db.cuentas_bancarias.findOne({
        include: [
            {
                model: db.usuario,
                attributes: ['email']
            },
            {
                model: db.bancos,
                attributes: ['banco_shortname']
            }
        ],
        attributes:[
            "slug_bank",
            "titular",
            "nro_cuenta",
            "moneda"
        ],
        where:{ 
            email:'guillermojuica3@gmail.com',
            slug_bank:slug_bank,
            moneda:moneda
        }
    }).then(bancos => {
        if (bancos) {
            res.json({ status:true , message:'Mostrando información', info:bancos })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'accountbank', verifyToken, (req, res) =>{
    const { email , moneda } = req.body;
    db.cuentas_bancarias.findAll({
        include: [
            {
                model: db.usuario,
                attributes: ['email']
            },
            {
                model: db.bancos,
                attributes: ['banco_shortname']
            }
        ],
        attributes:[
            "slug_bank",
            "titular",
            "nro_cuenta",
            "moneda"
        ],
        where:{ 
            email:email,
            moneda:moneda
        }
    }).then(bancos => {
        if (bancos) {
            res.json({ status:true , message:'Mostrando información', info:bancos })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'mybanks', verifyToken, (req, res) =>{
    const { email } = req.body;
    db.cuentas_bancarias.findAll({
        include: [
            {
                model: db.usuario,
                attributes: ['email']
            },
            {
                model: db.bancos
            }
        ],
        attributes:[
            "id_cuenta_bancaria",
            "slug_bank",
            "titular",
            "nro_cuenta",
            "moneda",
            'titular'
        ],
        where:{ 
            email:email
        }
    }).then(bancos => {
        if (bancos) {
            res.json({ status:true , message:'Mostrando información', info:bancos })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'addacountbank', verifyToken, (req, res) =>{
    const { email, slug_bank, nro_cuenta, moneda, titular } = req.body;
    db.cuentas_bancarias.create({
        email:email,
        slug_bank:slug_bank,
        nro_cuenta:nro_cuenta,
        moneda:moneda,
        titular:titular
    }).then(accountbank => {
        if (accountbank) {
            res.json({ status:true , message:'Cuenta bancaria agregado', info:accountbank })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'deleteaccountbank', verifyToken, (req, res) =>{
    const { email, id } = req.body;
    db.cuentas_bancarias.destroy({
        where: {
            email: email,
            id_cuenta_bancaria: id
        }
    }).then(accountbank => {
        if (accountbank) {
            res.json({ status:true , message:'Cuenta bancaria eliminada correctamente', info:accountbank })
        }else{
            res.json({ status:false , message:'No se pudo eliminar la cuenta bancaria seleccionada' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})


module.exports = router;