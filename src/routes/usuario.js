const express = require('express');
const router  = express.Router();
const jwt = require('jsonwebtoken')
const mkdirp = require('mkdirp');
const passwordHash = require('password-hash');
const apiToken = require('../config/apiToken'); 
const verifyToken = require('../config/verifyToken') 
const uploadFiles = require('../modules/UploadFiles');
const MailTemplate = require('../modules/MailTemplate');
const MailTemplateRecoverPassword = require('../modules/MailTemplateRecoverPassword');
const db = require('./../config/sequelize');
const mail = require('./../config/mail');

const oEmail = new mail();

//=== fn(x) COMPRUEBA SI EL USUARIO EXISTE ===//
function comprobanteUser(email, callback){
    db.usuario.findOne({
        where:{ 
            email:email 
        },
        attributes:[
            'password',
            'estado'
        ]
    }).then(usuario => {
        return usuario ? callback(usuario) : callback(false);
    }).catch(error => {
        console.log(error.errors[0].message)
    })
}

//=== fn(x) PARA SETEAR NOMBRES E IMAGEN DEL USUARIO ===//
function internalUserDetails(email, callback){
    db.usuario.findOne({
        where:{ email:email },
        attributes:[
            'nombres',
            'apellidos',
            'email',
            'imgUser'
        ]
    }).then(usuario => {
        if (usuario) {
            jwt.sign({usuario}, process.env.SITE_KEY, (err, token) => {
                return callback(token)
            })
        }else{
            return callback(false)
        }
    }).catch(error => {
        console.log(error.errors[0].message)
    })
}

//=== fn(x) ENVIA EMAIL DE VERIFICACION ===//
function sendMailVerification(emailTo, cod, callback){
    let emailTemplate = MailTemplate(cod);
    let email ={
        from:"noreply@btcencompraventa.com",
        to:emailTo,
        subject:"Verificación de correo btcencompraventa",
        html:emailTemplate
    };
    oEmail.enviarCorreo(email);
    // return callback(true);
}

//=== fn(x) ENVIA EMAIL DE RESTABLECIMIENTO DE CONTRASEÑA ===//
function sendMailVerificationRecover(emailTo, cod, callback){
    let emailTemplate = MailTemplateRecoverPassword(cod);
    let email ={
        from:"noreply@btcencompraventa.com",
        to:emailTo,
        subject:"Solicitud de restablecimiento de contraseña",
        html:emailTemplate
    };
    oEmail.enviarCorreo(email);

    db.usuario.update({
        resetToken: cod
    },{ 
        where: {email:emailTo} 
    })
}

//=== CONFIGURACIÓN DE LOGIN ===//
router.post(process.env.URL_ROUTES+'login', (req, res) => {
    const {email, password} = req.body;
    comprobanteUser(email, function(result){
        if (result) {
            if (passwordHash.verify(password, result.password)) {
                internalUserDetails(email, function(userDetail){
                    if (result.estado > 0) {
                        res.json({ status:true , message:'Ingreso validado.', token:userDetail })
                    }else{
                        res.json({ status:true, message:'Validar el correo enviado.', token:userDetail })
                    }
                })
            }else{
                res.json({ status:false , message:'Contraseña incorrecta.' })
            }
        }else{
            res.json({ status:false , message:'Usuario no existe.' })
        }
    });
})

//=== REGISTRO ===//
router.post(process.env.URL_ROUTES+'signin', (req, res) => {
    const {nombres , apellidos, email, password} = req.body;
    db.usuario.create({
        nombres:nombres,
        apellidos:apellidos,
        email:email,
        password:passwordHash.generate(password),
        activacion:apiToken(email)
    }).then(usuario => {
        internalUserDetails(email, function(userDetail){
            res.json({ status:true , message:'Ingreso validado.', token:userDetail })
            sendMailVerification(email, apiToken(email))
        })
    }).catch(error => {
        console.log(error)
        res.json({ status:false, message:'Email ya se encuentra en uso.' })
    })
})

//=== FORGOT ===//
router.post(process.env.URL_ROUTES+'forgot_password', (req, res) => {
    const { emailRecover } = req.body;
    if(!(emailRecover)){
        return res.status(400).json({ message:'Email es requerido!' });
    }

    db.usuario.findOne({ where:{ email: emailRecover } })
    .then(function(user) {
        if(user !== null){
            let token = jwt.sign({ email: user.email }, 'recover', { expiresIn:'1h' })
            sendMailVerificationRecover(emailRecover, token)
            res.json({ status:true, data:user, message:'Se envio un email con la solicitud de recuperación de contraseña.' })
        }else{
            res.json({ status:false, message:'Email no encontrado' })
        }
    })
    .catch(function(err) {
        res.json({ status:false, message:'Ocurrio un error al buscar usuario, ponganse en contacto con un administrador.' })
    })
})

//=== RECOVER ===//
router.post(process.env.URL_ROUTES+'verify_permisse_recover', (req, res) => {
    // const { emailRecover } = req.body;
    // if(!(emailRecover)){
    //     return res.status(400).json({ message:'Email es requerido!' });
    // }

    // db.usuario.findOne({ where:{ email: emailRecover } })
    // .then(function(user) {
    //     if(user !== null){
    //         let token = jwt.sign({ email: user.email }, 'recover', { expiresIn:'1h' })
    //         sendMailVerificationRecover(emailRecover, token)
    //         res.json({ status:true, data:user, message:'Se envio un email con la solicitud de recuperación de contraseña.' })
    //     }else{
    //         res.json({ status:false, message:'Email no encontrado' })
    //     }
    // })
    // .catch(function(err) {
    //     res.json({ status:false, message:'Ocurrio un error al buscar usuario, ponganse en contacto con un administrador.' })
    // })
})

//=== VERIFICACION DE USUARIO ===//
router.post(process.env.URL_ROUTES+'activate', (req, res) => {
    const { cod } = req.body;
    db.usuario.findOne({
        where:{ activacion:cod }
    }).then(usuario => {
        db.usuario.update(
            { estado:1, updatedAt:new Date() },
            { where: {activacion:cod} }
        ).then(actualizado => {
            res.json({ status:true, message:'Usuario verificado' })
        })
    }).catch(error => {
        res.json({ status:false, message:'Usuario no registrado' })
    })
})

//=== REGISTRO CON REDES SOCIALES ===//
router.post(process.env.URL_ROUTES+'signinsocial', (req,res) => {
    const {nombres , apellidos, email = '', password, imgUser = ''} = req.body;
    comprobanteUser(email, function(result){
        if (result) {
            internalUserDetails(email, function(userDetail){
                res.json({ status:true , message:'Ingreso validado.', token:userDetail })
            })
        }else{
            db.usuario.create({
                nombres:nombres,
                apellidos:apellidos,
                email:email,
                estado:1,
                password:passwordHash.generate(password),
                imgUser:imgUser
            }).then(usuario => {
                internalUserDetails(email, function(userDetail){
                    res.json({ status:true , message:'Usuario registrado.', token:userDetail })
                })
            })
        }
    })
})

//=== OBTENER LOS DATOS DEL 1 PERSONAL ===//
router.post(process.env.URL_ROUTES+'personal', verifyToken, (req, res) => {
    const {email} = req.body;
    db.usuario.findOne({
        where:{ email:email },
        attributes:[
            'nombres',
            'apellidos',
            'email',
            'codigo_pais',
            'celular',
            'direccion',
            'imgUser',
            'typeDoc',
		    'numDoc',
            'reniecValidation',
            'imgIdentityFace',
            'imgIdentityStamp',
            'imgIdentitySelfie',
            'imgValidation',
            'estado'            
        ]
    }).then(usuario => {
        if (usuario) {
            res.json({ status:true , message:'Mostrando información', info:usuario })
        }else{
            res.json({ status:false , message:'Usuario no encontrado' })
        }
    }).catch(error => {
        res.json({ status:false , message:'Permisos restringidos' })
    })
})

//=== ACTUALIZACION DE DATOS DE 1 PERSONAL (FOTO) ===//
router.put(process.env.URL_ROUTES+'personal/:email', verifyToken, (req, res) => {
    const {nombres, apellidos, typeDoc ,numDoc , reniecValidation, codigo_pais, celular, direccion} = req.body;
    const { email } = req.params;
    let folder = 'di/'+apiToken(email+'-folder')+'/'; //CODIFICACION DEL NOMBRE DE LA CARPETA QUE SE USARA PARA GUARDAR LAS IMAGENES 
    let imgIdentityFace, imgIdentityStamp, imgIdentitySelfie = false; //SI NO SE ENVIA LAS IMAGENES SE CANCELARA LA ACTUALIZACION

    mkdirp('./public/'+folder).then(made =>{
        let array = {
            nombres:        nombres, 
            apellidos:      apellidos,
            typeDoc:        typeDoc,
            numDoc:         numDoc,
            reniecValidation:reniecValidation,
            codigo_pais:    codigo_pais,
            celular:        celular,
            direccion:      direccion
        }
        if(req.body.imgIdentityFace !== "false"){
            uploadFiles(
                req.files.imgIdentityFace, 
                folder, 
                function(response){
                    imgIdentityFace = true;
                    array.imgIdentityFace = process.env.LOCAL_URL+response.directorio
                },
                'cara',
            )
        }
        if(req.body.imgIdentityStamp !== "false"){
            uploadFiles(
                req.files.imgIdentityStamp, 
                folder, 
                function(response){
                    imgIdentityStamp = true;
                    array.imgIdentityStamp = process.env.LOCAL_URL+response.directorio
                },
                'sello'
            )
        }
        if(req.body.imgIdentitySelfie !== "false"){
            uploadFiles(
                req.files.imgIdentitySelfie, 
                folder, 
                function(response){
                    imgIdentitySelfie = true;
                    array.imgIdentitySelfie = process.env.LOCAL_URL+response.directorio
                },
                'selfie'
            )
        }
        if (imgIdentityFace && imgIdentityStamp && imgIdentitySelfie) {
            array.imgValidation = 1
        }

        db.usuario.update(array, {where: {email:email}}
        ).then(actualizado => {
            res.json({ status:true, message:'Usuario actualizado' })
        }).catch(error => {
            res.json({ status:false , message:error.errors[0].message })
        }) 
        
    })
})

module.exports = router;