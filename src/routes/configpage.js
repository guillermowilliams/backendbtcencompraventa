const express = require('express');
const router  = express.Router();
const verifyToken = require('../config/verifyToken') 
const db = require('./../config/sequelize');

function fnVerifyData(p_compra, p_venta, host_email, codigo_wallet, arrayCtsPen, arrayCtsUsd, callback){
    db.configpage.count({
        where: {
            id_configpage: 1
        }
    }).then(configpage=> {
        if (configpage <= 0) {
            db.configpage.create({
                p_compra: p_compra,
                p_venta: p_venta,
                host_email: host_email,
                host_wallet: codigo_wallet,
                cts_pen:arrayCtsPen,
                cts_usd:arrayCtsUsd
            }).then(configpage_crear => {
                return callback(true);
            }).catch(error => {
                return callback(false);
            })
        }else{
            db.configpage.update(
                {
                    p_compra: p_compra,
                    p_venta: p_venta,
                    host_email: host_email,
                    host_wallet: codigo_wallet,
                    cts_pen:arrayCtsPen,
                    cts_usd:arrayCtsUsd
                },
                { where: {id_configpage:1} }
            ).then(configpage_update => {
                return callback(true);
            }).catch(error => {
                return callback(false);
            })
        }
    }).catch(error => {
        console.log(error)
    })
}

function fnWalletRecurrente(codigo_wallet, email, id_cryptomoneda, callback){
    db.wallet.count({
        where:{ 
            email:email,
            codigo_wallet:codigo_wallet 
        }
    }).then(wallet=> {
        if (wallet == 0) {
            db.wallet.create({
                codigo_wallet:codigo_wallet, 
                email: email, 
                id_cryptomoneda: id_cryptomoneda
            }).then(walletCreate => {
                return callback(true);
            })
        }
    }).catch(error => {
        console.log(error)
    })
}

router.post(process.env.URL_ROUTES+'configpage', verifyToken, (req, res) =>{
    const { p_compra, p_venta, host_email, email_admin, codigo_wallet, arrayCtsPen, arrayCtsUsd } = req.body;

    fnVerifyData(p_compra, p_venta, host_email, codigo_wallet, arrayCtsPen, arrayCtsUsd, function(result){
        if(result){
            res.json({ status:true, message:'Datos actualizados correctamente' })
            fnWalletRecurrente(codigo_wallet, email_admin, 1, function(result){})
        }else{
            res.json({ status:false, message:'Error al actualizar datos' })
        }
    })
})

router.get(process.env.URL_ROUTES+'configpage_get', verifyToken, (req, res) =>{
    db.configpage.findOne({
        attributes:[
            "p_compra",
            "p_venta",
            "host_email",
            "host_wallet",
            "cts_pen",
            "cts_usd"
        ],
        where:{ 
            id_configpage:1
        }
    }).then(configpage => {
        res.json({ status:true , message:'Mostrando información', info:configpage })
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

module.exports = router;