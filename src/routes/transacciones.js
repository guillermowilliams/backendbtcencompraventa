const express = require('express');
const { Op } = require("sequelize");
const router  = express.Router();
const uploadFiles = require('../modules/UploadFiles');
const verifyToken = require('../config/verifyToken') 
const db = require('./../config/sequelize');
const { v4: uuidv4 } = require('uuid');

function fnWalletRecurrente(codigo_wallet, email, id_cryptomoneda, callback){
    db.wallet.count({
        where:{ 
            email:email,
            codigo_wallet:codigo_wallet 
        }
    }).then(wallet=> {
        let statusOperation = false;
        if (wallet == 0) {
            db.wallet.create({
                codigo_wallet:codigo_wallet, 
                email: email, 
                id_cryptomoneda: id_cryptomoneda
            }).then(walletCreate => {
                statusOperation = true
            }).catch(error => {
                statusOperation = false
            })
        }
        return callback(statusOperation);
    }).catch(error => {
        console.log(error)
    })
}

function fnBankRecurrente(email, slug_bank, nro_cuenta, moneda, callback){
    db.cuentas_bancarias.count({
        where:{ 
            email:email,
            slug_bank:slug_bank,
            nro_cuenta:nro_cuenta,
            moneda:moneda
        }
    }).then(cuentas_bancarias=> {
        let statusOperation = false;
        if (cuentas_bancarias == 0) {
            db.cuentas_bancarias.create({
                email:email,
                slug_bank:slug_bank,
                nro_cuenta:nro_cuenta,
                moneda:moneda
            }).then(AccountBankCreate => {
                statusOperation = true
            }).catch(error => {
                statusOperation = false
            })
        }
        return callback(statusOperation);
    }).catch(error => {
        console.log(error)
    })
}

function fnVerificarTransaccion(id_cotizacion, callback) {
    let validationTransaction = false;
    db.transacciones.count({
        where:{ id_cotizacion: id_cotizacion}
    }).then(function(count) {
        if (count <= 0) {
            validationTransaction = true;
        }
        return callback(validationTransaction);
    });
}

function fnValidateUser(email, callback) {
    let statusOperation = false;
    db.usuario.findOne({ 
        where:{
            email: email
        }
    })
    .then(function(user) {
        if(user.imgValidation == 1 && user.estado == 1){
            statusOperation = true;
        }
        return callback(statusOperation);
    })
    .catch(function(err) {
        console.log(err)
    })
}

function fnVerificarRangoCotizador(id_cotizacion, callback){
    let statusOperation = false;
    db.cotizacion.findOne({ 
        where:{
            id_cotizacion: id_cotizacion
        }
    })
    .then(function(cotizacion_rango){
        var nDate = new Date().toLocaleString({
            timeZone: "America/Lima"
        });
        var expireDate = new Date(cotizacion_rango.date_expired).toLocaleString({
            timeZone: "America/Lima"
        });

        if (nDate <= expireDate) {
            statusOperation = true;
        }
        return callback(statusOperation);
    })
}

function fnSetearHora(){
    let d = new Date();
        let day = d.getDate();
        let month = d.getMonth() + 1;
        let hours = d.getHours();
        let minutes = d.getMinutes();
        let seconds = d.getSeconds();
        day = (day < 10) ? '0' + day : day;
        month = (month < 10) ? '0' + month : month;
        hours = (hours < 10) ? '0' + hours : hours;
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        seconds = (seconds < 10) ? '0' + seconds : seconds;

        let fecha_hora_actual = d.getFullYear() +'-'+ month +'-'+ day +' ' +hours+ ':'+minutes+':'+seconds;
        return fecha_hora_actual;
}

router.post(process.env.URL_ROUTES+'transacciones', verifyToken, (req, res) =>{
    const { 
        id_cryptomoneda, 
        id_cotizacion,
        cantidad_transaccion, 
        total_transaccion, 
        codigo_wallet, 
        slug_bank, 
        cta_bank, 
        img_voucher, 
        email, 
        moneda, 
        tipo_transaccion
    } = req.body;
    let rutaImgVoucher = '';
    let imgVoucherCompra = false;

    fnValidateUser(email, function(result_validate_user){
        if (result_validate_user) {
            fnVerificarRangoCotizador(id_cotizacion, function(result_verificador_rango_cotizador){
                if (result_verificador_rango_cotizador) {
                    fnVerificarTransaccion(id_cotizacion, function(result_validation_transaccion){
                        if(result_validation_transaccion){

                            if(req.body.imgVoucherCompra !== "false"){
                                uploadFiles(
                                    req.files.imgVoucherCompra, 
                                    'voucher/', 
                                    function(response){
                                        imgVoucherCompra = true;
                                        rutaImgVoucher = process.env.LOCAL_URL+response.directorio;
                                    },
                                    uuidv4(),
                                )
                            }
                
                            if(tipo_transaccion == 'comprar'){
                                fnWalletRecurrente(codigo_wallet, email, id_cryptomoneda, function(result){})
                            }else{
                                imgVoucherCompra = true;
                                fnBankRecurrente(email, slug_bank, cta_bank, moneda, function(result){})
                            }

                            if(imgVoucherCompra){
                                db.transacciones.create({
                                    id_cryptomoneda: id_cryptomoneda, 
                                    id_cotizacion: id_cotizacion,
                                    cantidad_transaccion: cantidad_transaccion, 
                                    total_transaccion: total_transaccion, 
                                    codigo_wallet: codigo_wallet, 
                                    slug_bank: slug_bank, 
                                    cta_bank: cta_bank, 
                                    img_voucher: img_voucher, 
                                    email: email, 
                                    moneda: moneda, 
                                    tipo_transaccion: tipo_transaccion,
                                    img_voucher: rutaImgVoucher
                                }).then(transacciones => {
                                    res.send({ status:true, message:'Transaccion registrada', data:transacciones })
                                }).catch(error => {
                                    res.json({ status:false, message:'Error al registrar', textlink:"¿Desea generar otra cotización?", link:"/calculadora" })
                                })
                            }else{
                                res.send({ status:false, message:'La transacción no puede efectuarse hasta que logre subir un voucher', textlink:"¿Desea generar otra cotización?", link:"/calculadora" })
                            }
                            
                        }else{
                            res.send({ status:false, message:'La transacción ya fue registrada', textlink:"¿Desea generar otra cotización?", link:"/calculadora" })
                        }
                    })
                }else{
                    res.send({ status:false, message:'La fecha u hora de la cotización se encuentra expirada', textlink:"¿Desea generar otra cotización?", link:"/calculadora" })
                }
            })
        }else{
            res.send({ status:false, message:'Usuario no validado por favor complete su información', textlink:"¿Desea completar su información?", link:"/perfil" })
        }
    })
})

router.get(process.env.URL_ROUTES+'transacciones', verifyToken, (req, res) =>{
    const { 
        email,
        limites,
        desde,
        hasta
    } = req.query;

    let where = { email:email };
    if(desde !== ''){
        where = { 
            email:email,
            date_register:{
                [Op.between]: [desde, hasta]
            }
        }
    }

    db.transacciones.findAll({
        include: [
            {
                model: db.cryptomoneda,
                attributes:[
                    'descripcion_larga',
                    'descripcion_corta',
                    'logo'
                ],
            },
            {
                model: db.cotizacion,
                attributes:[
                    'valorcripto',
                    'valordolar'
                ],
            }
        ],
        where:where,
        limit:Number(limites),
        order:[
            ['date_register', 'DESC']
        ]
    }).then(transacciones => {
        if (transacciones) {
            res.json({ status:true , message:'Mostrando información', info:transacciones })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.get(process.env.URL_ROUTES+'transacciones_entrantes', verifyToken, (req, res) =>{
    const { 
        email,
        limites,
        desde,
        hasta,
        cbo_status
    } = req.query;

    let transaccion_status = { [Op.ne]:'completado' };
    if(cbo_status){
        transaccion_status = { [Op.eq]:cbo_status }
    }

    let where = { 
        status_transaccion: transaccion_status
    };

    if(desde !== ''){
        where = { 
            date_register:{
                [Op.between]: [desde, hasta]
            },
            status_transaccion: transaccion_status
        }
    }

    db.transacciones.findAll({
        include: [
            {
                model: db.cryptomoneda,
                attributes:[
                    'descripcion_larga',
                    'descripcion_corta',
                    'logo'
                ],
            },
            {
                model: db.cotizacion,
                attributes:[
                    'valorcripto',
                    'valordolar'
                ],
            }
        ],
        where:where,
        limit:Number(limites),
        order:[
            ['date_register', 'DESC']
        ]
    }).then(transacciones => {
        if (transacciones) {
            res.json({ status:true , message:'Mostrando información', info:transacciones })
        }else{
            res.json({ status:false , message:'No se encontraron resultados' })
        }
    }).catch(error => {
        res.json({ status:false , message:error })
    })
})

router.post(process.env.URL_ROUTES+'transacciones_confirmation', verifyToken, (req, res) =>{
    const { 
        id_cotizacion,
        status_transaccion, 
        observacion
    } = req.body;

    db.transacciones.update(
        {
            status_transaccion: status_transaccion, 
            observacion: observacion, 
            date_confirmation: fnSetearHora()
        },
        { where: {id_cotizacion:id_cotizacion} }
    ).then(transacciones => {
        res.send({ status:true, message:'Transaccion actualizada' })
    }).catch(error => {
        res.json({ status:false, message:'Error al actualizar'})
    })
})

module.exports = router;