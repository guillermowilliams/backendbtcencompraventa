const express = require('express');
const fileUpload = require('express-fileupload');
require('dotenv').config()
const app = express();

// Settings
app.set('port', 3002);

// Middlewares
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
app.use(express.static('public'));
app.use(fileUpload());
app.use(express.json());

//Routes
app.use(require('./src/routes/usuario'));
app.use(require('./src/routes/cotizacion'));
app.use(require('./src/routes/bancos'));
app.use(require('./src/routes/wallet'));
app.use(require('./src/routes/transacciones'));
app.use(require('./src/routes/configpage'));

app.listen(app.get('port'), () => {
    console.log('Server on port 3002')
})